if ($('#map').length) {
  function initMap() {
    var myMap = new ymaps.Map('map', {
        center: [59.884307, 30.389350],
        controls: ['zoomControl'],
        zoom: 14
      }, {
        searchControlProvider: 'yandex#search'
      }),

      // Создаём макет содержимого.
      MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
        '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
      ),
      myPlacemark1 = new ymaps.Placemark([59.884307, 30.389350], {
        hintContent: ''
      }, {
        iconLayout: 'default#image',
        iconImageHref: 'img/map-placemark.svg',
        iconImageSize: [48, 48],
        iconImageOffset: [-24, -48],
        balloonCloseButton: false,
        hideIconOnBalloonOpen: false,
        balloonOffset: [0, -15],
        balloonPanelMaxMapArea: 0
      });

    myMap.geoObjects
      .add(myPlacemark1);
  }
}