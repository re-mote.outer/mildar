$('.link').click(function(e) {
  e.preventDefault();
  var linkHref = $(this).attr('href');
  $('body').css('opacity', 0);
  setTimeout(function() {
    window.location.href = linkHref;
  }, 1000)
});

$(window).on('load', function() {
  $('.loader').fadeOut('fast');
});
