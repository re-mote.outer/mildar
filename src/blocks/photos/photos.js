$('.photos').slick({
  dots: true,
  arrows: false,
  infinite: false,
  autoplay: true,
  autoplaySpeed: 5000,
  slidesToShow: 1,
  responsive: [
    {
      breakpoint: 1199,
      settings: {
        fade: true,
      }
    },
  ]
});
