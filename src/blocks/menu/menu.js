$('.menu__btn').click(function (e) {
  e.preventDefault();
  setTimeout(function() {
    $('.menu__btn').toggleClass('menu__btn--active');
    $('.menu__content').toggleClass('menu__content--active');
    if ($(window).width() < 1200) {
      $('.header').toggleClass('header--menu');
      $('body').toggleClass('fixed');
    }
  }, 100);
});

$(document).click(function (event) {
  var $target = $(event.target);
  if (!$target.closest('.menu').length &&
    $('.menu__content').hasClass("menu__content--active")) {
      $('.menu__btn').removeClass('menu__btn--active');
      $('.menu__content').removeClass('menu__content--active');
      $('.header').removeClass('header--menu');
  }
});
