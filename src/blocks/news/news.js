/* Пример подгрузки новостей по кнопке */

$('.js-more-news-btn').click(function(e) {
  e.preventDefault();
  $('.js-more-news-wrapper').clone().removeClass('js-more-news-wrapper').insertBefore($('.js-more-news-btn-wrapper'));
});