if ($(window).width() >= 1200) {

  $('.showcase__wrapper').each(function () {
    var slider = $(this);
    var itemsNumber = slider.find('.showcase__item').length;
    slider.find('.showcase__item-container').css('min-width', $(window).width());
    if (itemsNumber > 1) {
      var prevArrow;
      var nextArrow;

      slider.on('init', function (slick) {
        prevArrow = slider.find('.slick-prev');
        nextArrow = slider.find('.slick-next');
        var nextSlideTitle = slider.find('.showcase__item').eq(1).find('.showcase__title').text();
        prevArrow.append('<div class="slick-arrow__title slick-arrow__title--prev"></div>');
        nextArrow.append('<div class="slick-arrow__title slick-arrow__title--next">' + nextSlideTitle + '</div>');
      });

      slider.slick({
        dots: true,
        fade: true,
        speed: 200,
        infinite: false,
        pauseOnHover: false,
        autoplay: true,
        autoplaySpeed: 4000,
        adaptiveHeight: true,
        slidesToShow: 1,
        easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)'
      });

      slider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {

        slider.find('.showcase__item').removeClass('showcase__item--prev showcase__item--next showcase__item--old');

        if (nextSlide > currentSlide) {
          slider.find('.showcase__item').eq(currentSlide).addClass('showcase__item--prev');
        }
        else if (nextSlide < currentSlide) {
          slider.find('.showcase__item').eq(currentSlide-1).addClass('showcase__item--old');
          slider.find('.showcase__item').eq(currentSlide).addClass('showcase__item--next');
        }
      });


      slider.on('afterChange', function (slick, currentSlide) {
        var slideIndex = currentSlide.currentSlide;
        var prevSlideTitle = slider.find('.showcase__item').eq(slideIndex - 1).find('.showcase__title').text();
        var nextSlideTitle = slider.find('.showcase__item').eq(slideIndex + 1).find('.showcase__title').text();
        nextArrow.find('.slick-arrow__title').hide().fadeIn(1000);
        prevArrow.find('.slick-arrow__title').hide().fadeIn(1000);
        prevArrow.find('.slick-arrow__title').text(prevSlideTitle);
        nextArrow.find('.slick-arrow__title').text(nextSlideTitle);
        slider.find('.showcase__item').css('transform', 'translateX(0px)');
      });
    }
  });
}

else {

  $('.showcase__wrapper').each(function () {
    var slider = $(this);
    var itemsNumber = slider.find('.showcase__item').length;
    if (itemsNumber > 1) {
  
      slider.slick({
        dots: true,
        infinite: false,
        autoplay: true,
        pauseOnHover: false,
        autoplaySpeed: 4000,
        adaptiveHeight: true,
        slidesToShow: 1,
        easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)'
      });
    }
  })
}
