 $('.pictures__big').slick({
   slidesToShow: 1,
   slidesToScroll: 1,
   arrows: false,
   fade: true,
   infinite: false,
   asNavFor: '.pictures__small'
 });

 $('.pictures__small').slick({
   slidesToShow: 2,
   slidesToScroll: 1,
   asNavFor: '.pictures__big',
   infinite: false,
   arrows: false,
   centerMode: true,
   focusOnSelect: true
 }).on('afterChange', function (slick, currentSlide) {
   $('.product__right-content').hide();
   $('.product__right-content').eq(currentSlide.currentSlide).fadeIn('slow');
   $('.product__mobile-top').hide();
   $('.product__mobile-top').eq(currentSlide.currentSlide).fadeIn('slow');
   $('.photos').slick('unslick');
   $('.photos').not('.slick-initialized').slick({
    dots: true,
    arrows: false,
    infinite: false,
    autoplay: true,
    autoplaySpeed: 5000,
    slidesToShow: 1,
    responsive: [
      {
        breakpoint: 1199,
        settings: {
          fade: true,
        }
      },
    ]
  });
 });

ScrollReveal().reveal(".product__right-content--front > *", {
  duration: 1000,
  opacity: 0,
  scale: 1,
  mobile: false,
  easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
  origin: 'bottom',
  distance: "100px",
  reset: false,
  viewFactor: .2,
  cleanup: true,
  interval: 50
});
