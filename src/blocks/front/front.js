ScrollReveal().reveal(".front__title", {
  duration: 1000,
  opacity: 0,
  scale: 1,
  mobile: false,
  easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
  origin: 'bottom',
  delay: 1000,
  distance: "100px",
  reset: false,
  viewFactor: .2,
  cleanup: true
});

ScrollReveal().reveal(".front__btn", {
  duration: 1000,
  opacity: 0,
  scale: 1,
  mobile: false,
  easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
  origin: 'bottom',
  delay: 1300,
  distance: "100px",
  reset: false,
  viewFactor: .2,
  cleanup: true
});

ScrollReveal().reveal(".front__sections-wrapper", {
  duration: 1000,
  opacity: 0,
  scale: 1,
  mobile: false,
  easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
  origin: 'right',
  delay: 2000,
  distance: "20%",
  reset: false,
  viewFactor: .2,
  cleanup: true,
  interval: 100
});

ScrollReveal().reveal(".front__section", {
  duration: 1000,
  opacity: 0,
  scale: 1,
  mobile: false,
  easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
  origin: 'right',
  delay: 2000,
  distance: "100%",
  reset: false,
  viewFactor: .2,
  cleanup: true,
  interval: 100
});

ScrollReveal().reveal(".header--main", {
  duration: 1000,
  opacity: 0,
  scale: 1,
  mobile: false,
  easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
  origin: 'top',
  delay: 3000,
  distance: "100%",
  reset: false,
  viewFactor: .2,
  cleanup: true,
  interval: 100
});
