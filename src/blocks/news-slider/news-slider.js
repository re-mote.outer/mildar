if ($(window).width() < 1200 && $(window).width() > 767) {
  
  $('.news-slider__content').slick({
    slidesToShow: 2,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 5000,
    dots: true
  })
}
