if ($('.select__input').length) {
    
  $('.select--country .select__input').change(function() {
    var countryVal = $(this).val();
    
    selectFilling = function(country) {
      $('.select--city').find('option').remove();
      for (var i = 0; i < country.length; i++) {
        $('.select--city').find('.select__input').append('<option value="' + country[i][1] + '">' + country[i][0] + '</option>')
      }
    }
    
    var citiesRu = [['Санкт-Петербург', 'spb'], ['Москва', 'msk'], ['Екатеринбург', 'ekb'], ['Нижний Новгород', 'nn'], ['Владивосток', 'vvo']];
    var citiesBy = [['Минск', 'msq'], ['Гродно', 'gna'], ['Брест', 'bqt']];
    var citiesKz = [['Алматы', 'kz1'], ['Жанаозен', 'kz2'], ['Актау', 'kz3']];
    
    switch(countryVal) {
     case 'ru':
      selectFilling(citiesRu)
      break;
     case 'by':
      selectFilling(citiesBy)
      break;
     case 'kz':
      selectFilling(citiesKz)
      break;
     default:
    }
    
  });
  
  $('.select__input').each(function() {
    
    var container = $(this).closest('.select');

    $(this).select2({
      minimumResultsForSearch: -1,
      width: '100%',
      dropdownParent: container
    });
    $(this).on('select2:open', function (e) {
    
      setTimeout(function() {
        const ps = new PerfectScrollbar('.select2-results__options', {
          minScrollbarLength: 60,
          swipeEasing: true
        });
      }, 100);
    });
  })
};



$('.location__content').each(function() {
  var locationContent = $(this);
  
  if ($(window).width() < 1200) {

    locationContent.on('init', function(slick) {
      locationContent.find('.slick-dots li:first-child').prepend('<div class="slick-dots__filler"></div>');
      setTimeout(function() {
        var dotOffset = locationContent.find('.slick-dots li.slick-active').position().left;
        locationContent.find('.slick-dots__filler').css('left', dotOffset);
      }, 500)
      setTimeout(function() {
        locationContent.find('.slick-dots__filler').addClass('slick-dots__filler--animated')
      }, 1000)
    });

    locationContent.on('afterChange', function(slick, currentSlide) {
      var dotOffset = locationContent.find('.slick-dots li.slick-active').position().left;
      locationContent.find('.slick-dots__filler').css('left', dotOffset);
    });
  }
  
  var locationSlides = 4;
  
  if (locationContent.closest('.location').hasClass('location--product')) {
    locationSlides = 2;
  }

  locationContent.slick({
    slidesToShow: locationSlides,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          arrows: false,
          dots: true
        }
      },
      {
        breakpoint: 568,
        settings: {
          slidesToShow: 2,
          arrows: false,
          dots: true
        }
      }
    ]
  });
});


/* Временный скрипт для визуализации подгрузки магазинов */

$('.select--city .select__input').change(function() {
  $('.location__content').hide();
  $('.location__item').show();
  var itemsLength = $('.location__item').length;
  const rndInt = Math.floor(Math.random() * itemsLength+1) + 1;
  $('.location__item:nth-child(n+' + rndInt + ')').hide();
  $('.location__content').fadeIn('slow').slick({
    slidesToShow: 4,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          arrows: false,
          dots: true
        }
      },
      {
        breakpoint: 568,
        settings: {
          slidesToShow: 2,
          arrows: false,
          dots: true
        }
      }
    ]
  });
});
