var dotOffset;

$('.slider').each(function() {
  var sliderItem = $(this);

  sliderItem.on('init', function(slick) {
    sliderItem.find('.slick-dots li:first-child').prepend('<div class="slick-dots__filler"></div>');
    setTimeout(function() {
      var dotOffset = sliderItem.find('.slick-dots li.slick-active').position().left;
      sliderItem.find('.slick-dots__filler').css('left', dotOffset);
    }, 500)
    setTimeout(function() {
      sliderItem.find('.slick-dots__filler').addClass('slick-dots__filler--animated')
    }, 1000)
  });

  sliderItem.on('afterChange', function(slick, currentSlide) {
    var dotOffset = sliderItem.find('.slick-dots li.slick-active').position().left;
    sliderItem.find('.slick-dots__filler').css('left', dotOffset);
  });
});

if ($(window).width() > 767) {
  $('.article__news').slick({
    dots: true,
    arrows: false,
    infinite: false,
    autoplay: true,
    autoplaySpeed: 5000,
    slidesToShow: 2,
    slidesToScroll: 2
  });
}
