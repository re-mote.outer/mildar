$('.gallery').slick({
  dots: true,
  arrows: false,
  infinite: false,
  centerMode: true,
  autoplay: true,
  autoplaySpeed: 5000,
  variableWidth: true,
  infinite: true,
  responsive: [
    {
      breakpoint: 1199,
      settings: {
        slidesToShow: 1,
        fade: true,
        variableWidth: false,
        centerMode: false
      }
    },
  ]
});
