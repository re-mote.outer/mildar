$('.search__btn').click(function(e) {
  e.preventDefault();
  $('.search__content').addClass('search__content--active');
  $('.wrapper').addClass('wrapper--search');
  $('.header').addClass('header--search');
  $('body').addClass('fixed');
});

$('.search__close').click(function(e) {
  e.preventDefault();
  $('.search__content').removeClass('search__content--active');
  $('.search__results').removeClass('search__results--active');
  $('.wrapper').removeClass('wrapper--search');
  $('.header').removeClass('header--search');
  $('body').removeClass('fixed');
  $('.search__input').val('');
});

$('.search__results').click(function() {
  if (!$(this).hasClass('search__results--active')) {
    $('.search__content').removeClass('search__content--active');
    $('.wrapper').removeClass('wrapper--search');
    $('.header').removeClass('header--search');
    $('body').removeClass('fixed');
  }
})


/* Скриптs для демонстрации работы поиска */

$('.search__input').on('input', function() {
  if ($(this).val) {
    $('.search__results').addClass('search__results--active');
    $('.search__content').addClass('search__content--results');
  }
  else {
    $('.search__results').removeClass('search__results--active');
    $('.search__content').removeClass('search__content--results');
  }
});

$('.js-more-btn').click(function(e) {
  e.preventDefault();
  $(this).fadeOut('fast');
  $('.js-more-wrapper').find('.js-more-item').fadeIn('fast');
});
