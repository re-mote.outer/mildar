Fancybox.bind("[data-fancybox]", {
  dragToClose: false,
  afterShow: function (instance, current) {
    $(document).on('submit', '.needs-validation', function (e) {
      var form = $(this);
      if (!form[0].checkValidity()) {
        e.preventDefault();
        e.stopPropagation();
        $(this).addClass('was-validated');
      } else if (formIsValid($(this).attr('id'))) {
        sendData($(this).attr('id'));
        e.preventDefault();
        $(this).closest('.modal').find('.modal__content').hide();
        $(this).closest('.modal').find('.modal__result').fadeIn();
      }
    });
  }
});


$(document).on('submit', '.needs-validation', function (e) {
  var form = $(this);
  if (!form[0].checkValidity()) {
    e.preventDefault();
    e.stopPropagation();
    $(this).addClass('was-validated');
  }
});