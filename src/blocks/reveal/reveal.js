ScrollReveal().reveal(".reveal", {
  duration: 1000,
  opacity: 0,
  scale: 1,
  mobile: false,
  easing: 'cubic-bezier(0.250, 0.100, 0.250, 1.000)',
  origin: 'bottom',
  distance: "100px",
  reset: false,
  viewFactor: .2,
  cleanup: true,
  interval: 50
});
